warnings.filterwarnings('ignore')

''' TEMPORARILY USE FOR DEBUG --------------------------------------------------------
Input:
0: string, TIF image path
1: string, Cropped 500*500px QR code taken path
2: string, QR url
3: float, Edge_width
4: float, XY_Edge_width
---
Output:
0: int, validation result: 0-fake, 1-real, -2-not sure image, 2-unexpected error 
1: string, log message
-------------------------------------------------------------------------------------'''


def QRValidate_debug(*args):
    if not len(args) == 5:
        return 2, 'Invalid arguments!'
    img_source_string = args[0]
    img_cropped_string = args[1]
    url = args[2]
    EW = float(args[3])
    XY_EW = float(args[4])

    out_string = img_source_string + "\n" + img_cropped_string + "\n" + url + "\n" + str(EW) + "\n" + str(XY_EW) + "\n"

    if EW == XY_EW:
        return 1, 'equal values.\n' + out_string
    else:
        return 0, 'not equal values.\n' + out_string


''' TEMPORARILY USE FOR DEBUG --------------------------------------------------------
Input:
0: string, user upload image path
[ ----OPTIONAL---- ]
[1: string, QR position reading by ZXing]
[2: string, QR url]
[3: float, scaled width]
[4: float, scaled height]
---
Output:
0: int, IQA check result: 1-success, 2-unexpected error, 5:12-IQA check failed 
1: string, log message
2: sring, cropped image path, can be argv[1] of QRValidate() in case if IQA check success
3: float, Edge_width,  can be argv[2] of QRValidate() in case if IQA check success
4: float, XY_Edge_width,  can be argv[3] of QRValidate() in case if IQA check success
-------------------------------------------------------------------------------------'''


def QRCrop_debug(*args):
    if len(args) == 1:
        imName = args[0]
    elif len(args) == 5:
        imName = args[0]
        qr_position = args[1]
        url = args[2]
        scaled_width = args[3]
        scaled_height = args[4]
    else:
        return 2, 'Invalid arguments!', 'None', -1, -1

        return 1, "Check succes!\n", imName + '_cropped.jpg', 5.99, 9.55


def bootstrap(file):
    QRValidate_debug(file)
