import torch
import torch.nn as nn
from torchvision.models import vgg16_bn
import encoding
from torch.autograd import Variable
from PIL import Image
import torchvision
from torchvision import transforms


class SiameseNetwork(nn.Module):
    def __init__(self, embedding_size=512, numClass=3000, pretrained=True):
        super(SiameseNetwork, self).__init__()
        self.embedding_size = embedding_size
        self.numClass = numClass
        self.embeddingnet = vgg16_bn(pretrained)
        self.embeddingnet.classifier = nn.Linear(self.embedding_size, self.numClass)

        self.embedding = nn.Sequential(
            # nn.Linear(104960, 2048),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(1024 * 64, self.embedding_size),
        )
        # self.embeddingnet.features[33] = nn.MaxPool2d(kernel_size=4, stride=4)
        self.embeddingnet.features[33] = nn.AdaptiveAvgPool2d((7, 7))
        self.embeddingnet.features[43] = nn.AdaptiveAvgPool2d((7, 7))

        self.encode = nn.Sequential(
            encoding.nn.Encoding(D=1024, K=64),
            encoding.nn.View(-1, 1024 * 64),
            # encoding.nn.Normalize(),
            # nn.Linear(512*self.n_codes, self.nclass),
        )

    def l2_norm(self, input):
        input_size = input.size()
        buffer = torch.pow(input, 2)

        normp = torch.sum(buffer, 1).add_(1e-10)
        norm = torch.sqrt(normp)

        _output = torch.div(input, norm.view(-1, 1).expand_as(input))

        output = _output.view(input_size)

        return output

    def forward(self, x):
        # try vgg16 bn
        x = self.embeddingnet.features[0](x)
        x = self.embeddingnet.features[1](x)
        x = self.embeddingnet.features[2](x)
        x = self.embeddingnet.features[3](x)
        x = self.embeddingnet.features[4](x)
        x = self.embeddingnet.features[5](x)
        x = self.embeddingnet.features[6](x)
        x = self.embeddingnet.features[7](x)
        x = self.embeddingnet.features[8](x)
        x = self.embeddingnet.features[9](x)
        x = self.embeddingnet.features[10](x)
        x = self.embeddingnet.features[11](x)
        x = self.embeddingnet.features[12](x)
        x = self.embeddingnet.features[13](x)
        x = self.embeddingnet.features[14](x)
        x = self.embeddingnet.features[15](x)
        x = self.embeddingnet.features[16](x)
        x = self.embeddingnet.features[17](x)
        x = self.embeddingnet.features[18](x)
        x = self.embeddingnet.features[19](x)
        x = self.embeddingnet.features[20](x)
        x = self.embeddingnet.features[21](x)
        x = self.embeddingnet.features[22](x)
        x = self.embeddingnet.features[23](x)
        x = self.embeddingnet.features[24](x)
        x = self.embeddingnet.features[25](x)
        x = self.embeddingnet.features[26](x)
        x = self.embeddingnet.features[27](x)
        x = self.embeddingnet.features[28](x)
        x = self.embeddingnet.features[29](x)
        x = self.embeddingnet.features[30](x)
        x = self.embeddingnet.features[31](x)
        x = self.embeddingnet.features[32](x)
        x1 = self.embeddingnet.features[33](x)

        x = self.embeddingnet.features[34](x1)
        x = self.embeddingnet.features[35](x)
        x = self.embeddingnet.features[36](x)
        x = self.embeddingnet.features[37](x)
        x = self.embeddingnet.features[38](x)
        x = self.embeddingnet.features[39](x)
        x = self.embeddingnet.features[40](x)
        x = self.embeddingnet.features[41](x)
        x = self.embeddingnet.features[42](x)
        x2 = self.embeddingnet.features[43](x)

        # print(x1.shape)
        # print(x2.shape)
        xc = torch.cat((x1, x2), 1)
        xe = self.encode(xc)

        xf = self.embedding(xe)
        features_x = self.l2_norm(xf)
        # Multiply by alpha = 10 as suggested in https://arxiv.org/pdf/1703.09507.pdf
        alpha = 10
        features_x = features_x * alpha
        return features_x

    def forward_classifier(self, x):
        x = self.embeddingnet.classifier(x)
        return x


def image_loader(image_name):
    """load image, returns cuda tensor"""
    image = Image.open(image_name)
    image = loader(image).float()
    image = Variable(image, requires_grad=False)
    image = image.unsqueeze(0)  # this is for VGG, may not be needed for ResNet
    return image  # assumes that you're using GPU


loader = transforms.Compose([
    torchvision.transforms.Resize((448, 448)),
    transforms.ToTensor(),
    transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
])

model = SiameseNetwork(embedding_size=1024)
for param in model.parameters():
    param.requires_grad = False
model.embedding = nn.Sequential(
    # nn.Linear(104960, 2048),
    nn.ReLU(True),
    nn.Dropout(),
    nn.Linear(1024 * 64, 1024),
)
model.embeddingnet.classifier = nn.Linear(1024, 1812)
model.load_state_dict(
    torch.load('Siamese_VGG_2Level_DeepTEN64_54epoch_Transfer2Pump_40epoch_Crop.pth', map_location='cpu'))
model.eval()


def bootstrap(file):
    img = image_loader(file)
    output = model.forward(img)
    feature = output.data.numpy()
    print(feature)
