import pymysql

database = pymysql.connect("127.0.0.1", "root", "123456", "mall", charset='utf8')
cursor = database.cursor()
# sql = "INSERT INTO mall.ums_member_tag (name,finish_order_count,finish_order_amount) VALUES('eee','6000','88')"
sql = "SELECT name,finish_order_count,finish_order_amount FROM mall.ums_member_tag"
cursor.execute(sql)

# (('11', 6000, Decimal('88.00')), ('eee', 6000, Decimal('88.00')))
print(cursor.fetchall())
database.commit()
database.close()
