import sys


def func(name):
    return "Hello," + name


if __name__ == '__main__':

    a = []
    for i in range(1, len(sys.argv)):
        a.append((sys.argv[i]))

    print(func(a[0]))
