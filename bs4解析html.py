import requests
from bs4 import BeautifulSoup
import re

url = "http://python123.io/ws/demo.html"

demo = requests.get(url)
soup = BeautifulSoup(demo.text, "html.parser")
print(soup.find_all(string='Basic Python'))
