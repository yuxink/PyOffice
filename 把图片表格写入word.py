from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Pt
from docx.oxml.ns import qn
from docx.shared import Inches
import time

price = input('请输入价格')
company_list = ['客户11', '客户12', '客户13', '客户14', '客户15']
# today1 = time.strftime("%Y-%m-%d", time.localtime())
today = time.strftime("%Y{y}%m{m}%d{d}", time.localtime()).format(y='年', m='月', d='日')
print(today)

for i in company_list:
    document = Document()
    document.styles['Normal'].font.name = u'宋体'
    document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')

    document.add_picture('d:/logo.png', width=Inches(6))
    # 设置文档基础字体
    p1 = document.add_paragraph()
    # 初始化一个自然段
    p1.alignment = WD_ALIGN_PARAGRAPH.CENTER

    run1 = p1.add_run('关于%s产品价格通知' % (today))
    run1.font.name = '微软雅黑'
    run1.font.size = Pt(21)
    run1.font.bold = True

    p1.space_after = Pt(5)
    p1.space_before = Pt(5)

    p2 = document.add_paragraph()
    run2 = p2.add_run(i + ': ')
    run2.font.name = '仿宋_GB2312'
    run2._element.rPr.rFonts.set(qn('w:eastAsia'), u'仿宋_GB2312')
    run2.font.size = Pt(16)
    run2.font.bold = True

    p3 = document.add_paragraph()
    run3 = p3.add_run('     今日的价格是%s元' % price)
    run3.font.name = '仿宋_GB2312'
    run3._element.rPr.rFonts.set(qn('w:eastAsia'), u'仿宋_GB2312')
    run3.font.size = Pt(16)
    run3.font.bold = True

    table = document.add_table(rows=3, cols=3, style='Table Grid')
    table.cell(0, 0).merge(table.cell(0, 2))
    table_run1 = table.cell(0, 0).paragraphs[0].add_run('XX产品报价表')
    table_run1.font.name = u'隶书'
    table_run1._element.rPr.rFonts.set(qn('w:eastAsia'), u'隶书')
    table.cell(0, 0).paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER

    table.cell(1, 0).text = '日期'
    table.cell(1, 1).text = '价格'
    table.cell(1, 2).text = '备注'
    table.cell(2, 0).text = today
    table.cell(2, 1).text = str(price)
    table.cell(2, 2).text = 'xxx'

    p4 = document.add_paragraph()
    run4 = p4.add_run(' 联系人：小于  电话：18888888888 ')
    run4.font.name = '仿宋_GB2312'
    run4._element.rPr.rFonts.set(qn('w:eastAsia'), u'仿宋_GB2312')
    run4.font.size = Pt(16)
    run4.font.bold = True

    document.add_page_break()
    document.save('d:/%s-通知.docx' % i)
